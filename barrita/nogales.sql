-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-05-2020 a las 01:25:43
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nogales`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `cat_id` int(2) NOT NULL,
  `cat_nombre` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `cat_foto` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `a` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`cat_id`, `cat_nombre`, `cat_foto`, `a`) VALUES
(1, 'Semillas', 'images/slider/semillas.png', 'semillas.php'),
(2, 'Legumbres', 'images/slider/legumbres.png', 'legumbres.php'),
(3, 'Frutos Secos', 'images/slider/secos.png', 'frutossecos.php'),
(4, 'Sin tacc', 'images/slider/sintacc.png', 'sintacc.php'),
(5, 'Mixes', 'images/slider/mixes.png', 'mixes.php'),
(6, 'Veganos', 'images/slider/veganos.png', 'veganos.php'),
(7, 'Lasfor', 'images/slider/lasfor.png', 'lasfor.php'),
(8, 'Granolas', 'images/slider/granolas.png', 'granolas.php'),
(9, 'Frutas sin Azucar', 'images/slider/sinazucar.png', 'sinazucar.php'),
(10, 'Frutas Glaseadas', 'images/slider/glaseadas.png', 'glaseadas.php'),
(11, 'Snacks', 'images/slider/snacks.png', 'snacks.php'),
(12, 'Aceites Vegetalos', 'images/slider/aceites.jpg', 'aceites.php'),
(13, 'Sales', 'images/slider/sales.png', 'sales.php'),
(14, 'Endulzantes', 'images/slider/endulzantes.png', 'endulzantes.php'),
(15, 'Confituras', 'images/slider/confituras.jpg', 'confituras.php'),
(16, 'Reposteria', 'images/slider/reposteria.jpg', 'reposteria.php'),
(17, 'Yerbas', 'images/slider/yerba.jpg', 'yerbas.php'),
(18, 'Galletas de Arroz', 'images/slider/arroz.jpg', 'arroz.php'),
(19, 'Harinas y Feculas', 'images/slider/harinas.jpg', 'harinas.php'),
(20, 'Leches Vegetales', 'images/slider/leches.jpg', 'leches.php'),
(21, 'Avenas', 'images/slider/avenas.jpg', 'avenas.php'),
(22, 'Jugos Naturales', 'images/slider/jugos.jpg', 'jugos.php'),
(23, 'Condimentos y Especias', 'images/slider/condimentos.jpg', 'condimentos.php'),
(24, 'Vegetales y Hongos', 'images/slider/verduras.jpg', 'verduras.php'),
(25, 'Arroz', 'images/slider/arroces.jpg', 'arroces.php'),
(26, 'Productos de Coco', 'images/slider/coco.jpg', 'coco.php'),
(27, 'Fideos y Arroces', 'images/slider/fideos.jpg', 'fideos.php'),
(28, 'Soja', 'images/slider/soja.jpg', 'soja.php');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Comentario` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `Nombre`, `Email`, `Comentario`) VALUES
(1, 'Rolando', 'rolandogimenez0@gmail.com', 'T quiero vender cocacola'),
(2, 'luisito', 'luisvideoar@gmail.com', 't quiero vender pepsi'),
(3, 'Rolando', 'rolandogimenez0@gmail.com', '1662626'),
(4, 'Rolando', 'elviralamisionera@hotmail.com', ''),
(5, 'rolando', 'rolandogimenez@gmail.com', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ofertas`
--

CREATE TABLE `ofertas` (
  `codigoComida` int(11) NOT NULL,
  `prd_nombre` varchar(50) NOT NULL,
  `prd_descripcion` text NOT NULL,
  `prd_precio` int(5) NOT NULL,
  `cat_id` int(2) NOT NULL,
  `prd_foto1` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ofertas`
--

INSERT INTO `ofertas` (`codigoComida`, `prd_nombre`, `prd_descripcion`, `prd_precio`, `cat_id`, `prd_foto1`) VALUES
(91, 'Mix de frutas cubeteadas', 'Presentación x 500g(durazno,damasco,ciruela y pera)', 106, 5, 'images/mixes/pic03.jpg'),
(132, 'Granola especial', 'Presentación x 1kg\r\nAvena arrollada, copos de maiz, sésamo integral, lino, girasol, almendra, caju, coco rallado, miel, pasas de uva morochas y rubias, esencia de vainilla y canela molida. Con un 35% mas de frutos secos', 320, 8, 'images/granolas/pic06.jpg'),
(371, 'Mix de 4 semillas', 'Presentación x 1kg (lino, sesamo integral, girasol, chia)', 200, 1, 'images/semillas/pic16.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `codigoComida` int(3) NOT NULL,
  `prd_nombre` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `prd_descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `prd_precio` int(5) NOT NULL,
  `cat_id` int(2) NOT NULL,
  `prd_alta` date NOT NULL,
  `prd_foto1` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'sin-foto.png',
  `prd_foto2` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT 'sin-foto2.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`codigoComida`, `prd_nombre`, `prd_descripcion`, `prd_precio`, `cat_id`, `prd_alta`, `prd_foto1`, `prd_foto2`) VALUES
(1, 'Chia', 'Presentación x 500g', 130, 1, '2012-01-05', 'images/Semillas/pic02.jpg', 'sin-foto2.png'),
(2, 'Lino Marrón', 'Presentación x 500g', 70, 1, '2012-01-07', 'images/Semillas/pic03.jpg', 'sin-foto2.png'),
(3, 'Lino Dorado', 'Presentación x 500g', 85, 1, '2012-01-10', 'images/Semillas/pic04.jpg', 'sin-foto2.png'),
(4, 'Sesamo blanco', 'Presentación x 500g', 280, 1, '2012-01-10', 'images/Semillas/pic05.jpg', 'sin-foto2.png'),
(5, 'Sesamo Negro (India)', 'Presentación x 500g', 320, 1, '2012-01-16', 'images/Semillas/pic06.jpg', 'sin-foto2.png'),
(6, 'Girasol Pelado', 'Presentación x 500g', 238, 1, '2012-01-16', 'images/Semillas/pic07.jpg', 'sin-foto2.png'),
(7, 'Zapallo Pelada', 'Presentación x 500g', 366, 1, '2012-01-25', 'images/Semillas/pic08.jpg', 'sin-foto2.png'),
(8, 'Quinoa Blanca Real', 'Presentación x 500g', 270, 1, '2012-01-30', 'images/Semillas/pic09.jpg', 'sin-foto2.png'),
(9, 'Amapola Azul', 'Presentación x 250g', 250, 1, '2012-01-30', 'images/Semillas/pic10.jpg\r\n\r\n\r', 'sin-foto2.png'),
(10, 'Amaranto', 'Presentación x 500g', 160, 1, '2012-01-30', 'images/Semillas/pic11.jpg\r\n\r\n', 'sin-foto2.png'),
(11, 'Mijo Pelado', 'Presentación x 500g', 160, 1, '2012-02-01', 'images/Semillas/pic12.jpg\r\n', 'sin-foto2.png'),
(12, 'Mostaza Amarilla', 'Presentación x 500g', 120, 1, '2012-02-01', 'images/Semillas/pic13.jpg\r\n', 'sin-foto2.png'),
(13, 'Girasol Pelado Tostado Con Sal', 'Presentación x 500g', 122, 1, '2012-02-08', 'images/Semillas/pic14.jpg\r\n', 'sin-foto2.png'),
(39, 'Lentejon N°2 (Canada)', 'Presentacion x: 500g', 135, 2, '0000-00-00', 'images/Legumbres/pic02.jpg\r\n\r\n', 'sin-foto2.png'),
(40, 'Lenteja Naranja (Turca)', 'Presentacion x: 500g', 200, 2, '0000-00-00', 'images/Legumbres/pic03.jpg\r\n', 'sin-foto2.png'),
(41, 'Garbanzo 7mm', 'Presentacion x: 500g', 30, 2, '0000-00-00', 'images/Legumbres/pic04.jpg\r\n', 'sin-foto2.png'),
(42, 'Garbanzo 9mm', 'Presentacion x: 500g', 52, 2, '0000-00-00', 'images/Legumbres/pic05.jpg\r\n', 'sin-foto2.png'),
(43, 'Arveja seca partida', 'Presentacion x: 500g', 45, 2, '0000-00-00', 'images/Legumbres/pic06.jpg\r\n\r\n', 'sin-foto2.png'),
(44, 'Poroto Regina', 'Presentacion x: 500g', 82, 2, '0000-00-00', 'images/Legumbres/pic07.jpg\r\n', 'sin-foto2.png'),
(45, 'Poroto Mung', 'Presentacion x: 500g', 86, 2, '0000-00-00', 'images/Legumbres/pic08.jpg\r\n', 'sin-foto2.png'),
(46, 'Poroto alubia', 'Presentacion x: 500g', 81, 2, '0000-00-00', 'images/Legumbres/pic09.jpg\r\n', 'sin-foto2.png'),
(47, 'Poroto negro', 'Presentacion x: 500g', 70, 2, '0000-00-00', 'images/Legumbres/pic10.jpg\r\n', 'sin-foto2.png'),
(48, 'Poroto colorados', 'Presentacion x: 500g', 74, 2, '0000-00-00', 'images/Legumbres/pic11.jpg\r\n', 'sin-foto2.png'),
(49, 'Poroto de soja', 'Presentacion x: 500g', 35, 2, '0000-00-00', 'images/Legumbres/pic12.jpg\r\n', 'sin-foto2.png'),
(50, 'Porotos pallares', 'Presentacion x: 500g', 139, 2, '0000-00-00', 'images/Legumbres/pic13.jpg\r\n', 'sin-foto2.png'),
(51, 'Porotos Aduki', 'Presentacion x: 500g', 70, 2, '0000-00-00', 'images/Legumbres/pic14.jpg\r\n', 'sin-foto2.png'),
(53, 'Nueces peladas blancas mariposa (extra light) Chan', 'Presentacion x: 500g\r\n\r\n', 750, 3, '0000-00-00', 'images/Secos/pic02.jpg', 'sin-foto2.png'),
(54, 'Nueces peladas doradas mariposa', 'Presentacion x: 500g\r\n\r\n', 620, 3, '0000-00-00', 'images/Secos/pic03.jpg', 'sin-foto2.png'),
(55, 'Almendras peladas Non Pareil N°6 Calibre 23-25', 'Presentacion x: 500g\r\n\r\n', 950, 3, '0000-00-00', 'images/Secos/pic04.jpg', 'sin-foto2.png'),
(56, 'Almendras Carmel N°4', 'Presentacion x: 500g\r\n\r\n', 700, 3, '0000-00-00', 'images/Secos/pic05.jpg', 'sin-foto2.png'),
(57, 'Almendras con cascara', 'Presentacion x: 500g\r\n\r\n', 325, 3, '0000-00-00', 'images/Secos/pic06.jpg', 'sin-foto2.png'),
(58, 'Avellanas tostadas', 'Presentacion x: 500g\r\n\r\n', 750, 3, '0000-00-00', 'images/Secos/pic07.jpg', 'sin-foto2.png'),
(59, 'Avellanas chicas', 'Presentacion x: 500g\r\n\r\n', 1000, 3, '0000-00-00', 'images/Secos/pic08.jpg', 'sin-foto2.png'),
(60, 'Castañas de Caju W4 (Brasil) tostadas', 'Presentacion x: 500g\r\n\r\n', 800, 3, '0000-00-00', 'images/Secos/pic09.jpg', 'sin-foto2.png'),
(61, 'Castañas de Caju W4 (Brasil) tostadas y saladas', 'Presentacion x: 500g\r\n\r\n', 800, 3, '0000-00-00', 'images/Secos/pic10.jpg', 'sin-foto2.png'),
(62, 'Castañas de Caju W4 (Brasil) cruda entera', 'Presentacion x: 500g\r\n\r\n', 745, 3, '0000-00-00', 'images/Secos/pic11.jpg', 'sin-foto2.png'),
(63, 'Castañas de Caju P3 partidas crudas', 'Presentacion x: 500g\r\n\r\n', 590, 3, '0000-00-00', 'images/Secos/pic12.jpg', 'sin-foto2.png'),
(64, 'Castañas de para partidas', 'Presentacion x: 500g\r\n\r\n', 520, 3, '0000-00-00', 'images/Secos/pic13.jpg', 'sin-foto2.png'),
(65, 'Castañas de Para entera natural', 'Presentacion x: 500g\r\n\r\n', 800, 3, '0000-00-00', 'images/Secos/pic14.jpg', 'sin-foto2.png'),
(66, 'Pistachos con cascara tostados y salados GRANDE', 'Presentacion x: 500g\r\n\r\n', 1000, 3, '0000-00-00', 'images/Secos/pic15.jpg', 'sin-foto2.png'),
(67, 'Pistachos pelados naturales', 'Presentacion x: 500g\r\n\r\n', 1500, 3, '0000-00-00', 'images/Secos/pic16.jpg', 'sin-foto2.png'),
(68, 'Mani blancheado repelado tostado con sal', 'Presentacion x: 500g\r\n\r\n', 100, 3, '0000-00-00', 'images/Secos/pic17.jpg', 'sin-foto2.png'),
(69, 'Mani con vaina (Jumbo) Variedad Virginia', 'Presentacion x: 500g\r\n\r\n', 120, 3, '0000-00-00', 'images/Secos/pic18.jpg', 'sin-foto2.png'),
(70, 'Mani runner crudo', 'Presentacion x: 500g\r\n\r\n', 100, 3, '0000-00-00', 'images/Secos/pic19.jpg', 'sin-foto2.png'),
(71, 'Mani runner crudo', 'Presentacion x: 500g\r\n\r\n', 100, 3, '0000-00-00', 'images/Secos/pic20.jpg', 'sin-foto2.png'),
(72, 'Mani repelado crudo', 'Presentacion x: 500g\r\n\r\n', 90, 3, '0000-00-00', 'images/Secos/pic21.jpg', 'sin-foto2.png'),
(73, 'Mani Japones crocante', 'Presentacion x: 500g\r\n\r\n', 110, 3, '0000-00-00', 'images/Secos/pic22.jpg', 'sin-foto2.png'),
(74, 'Mani Japones horneado', 'Presentacion x: 500g\r\n\r\n', 250, 3, '0000-00-00', 'images/Secos/pic23.jpg', 'sin-foto2.png'),
(75, 'Galletas de arroz Carilo ', 'Presentación x 150 grs sin sal nuevas – SIN TACC', 56, 4, '0000-00-00', 'images/sintacc/pic01.png', 'sin-foto2.png'),
(76, 'Galletas de arroz Carilo ', 'Presentación x 150 grs dulces – SIN TACC', 56, 4, '0000-00-00', 'images/sintacc/pic02.jpg', 'sin-foto2.png'),
(77, 'Galletas de arroz Carilo', 'Presentación x 150 grs tradicional – SIN TACC', 56, 4, '0000-00-00', 'images/sintacc/pic03.jpg', 'sin-foto2.png'),
(78, 'Harina Integral trigo sarraceno SIN TACC ', 'Presentacion x 500 grs', 103, 4, '0000-00-00', 'images/sintacc/pic04.jpg', 'sin-foto2.png'),
(79, 'Levadura natural en copos', 'Presentacion x 200 grs', 479, 4, '0000-00-00', 'images/sintacc/pic05.jpg', 'sin-foto2.png'),
(80, 'Yerba mate organica RoaPipo', 'Presentación x 1 Kg', 344, 4, '0000-00-00', 'images/sintacc/pic06.jpg', 'sin-foto2.png'),
(81, 'Yerba mate organica RoaPipo ', 'Presentación x 500 grs – Tradicional', 169, 4, '0000-00-00', 'images/sintacc/pic07.jpg', 'sin-foto2.png'),
(82, 'Fideos de arroz tradicional', 'Presentación x 300g', 95, 4, '0000-00-00', 'images/sintacc/pic08.jpg', 'sin-foto2.png'),
(83, 'Fideos de arroz con maiz', 'Presentación x 300g', 95, 4, '0000-00-00', 'images/sintacc/pic09.jpg', 'sin-foto2.png'),
(84, 'Yerba mate Sin TACC Kalena', 'Presentación x 500g', 158, 4, '0000-00-00', 'images/sintacc/pic10.jpg', 'sin-foto2.png'),
(85, 'Almohaditas «Snuks» Rellenas de FRUTILLA SIN TACC', 'Presentación x 200 g', 101, 4, '0000-00-00', 'images/sintacc/pic11.jpg', 'sin-foto2.png'),
(86, 'Dulce de Leche c/Stevia ', 'Presentación x 400 grs – SIN TACC', 213, 4, '0000-00-00', 'images/sintacc/pic12.jpg', 'sin-foto2.png'),
(87, 'Dulce de Leche c/Stevia ', 'Presentación x 200 grs – SIN TACC', 155, 4, '0000-00-00', 'images/sintacc/pic13.jpg', 'sin-foto2.png'),
(88, 'Stevia Liquida', 'Presentación x 100 ml – SIN TACC', 122, 4, '0000-00-00', 'images/sintacc/pic14.jpg', 'sin-foto2.png'),
(89, 'Stevia en Polvo ', 'Presentación x 100 grs – SIN TACC', 206, 4, '0000-00-00', 'images/sintacc/pic15.jpg', 'sin-foto2.png'),
(90, 'Mix de compota', 'Presentación x 500g (pelónes,ciruela con carozo,damascos,peras e higos) ', 75, 5, '0000-00-00', 'images/mixes/pic02.jpg', 'sin-foto2.png'),
(91, 'Mix de frutas cubeteadas', 'Presentación x 500g(durazno,damasco,ciruela y pera)', 106, 5, '0000-00-00', 'images/mixes/pic03.jpg', 'sin-foto2.png'),
(92, 'Mix de frutas NAC & POP', 'Presentación x 500g (nuez,almendra,mani,banana y pasas)', 245, 5, '0000-00-00', 'images/mixes/pic04.jpg', 'sin-foto2.png'),
(93, 'Mix de frutas secas premium', 'Presentación x 500g (nuez,almendra,caju,pasa morocha y pasa rubia)', 282, 5, '0000-00-00', 'images/mixes/pic05.jpg', 'sin-foto2.png'),
(94, 'Mix de frutas secas con maní', 'Presentación x 500g (nuez,mani,almendra,caju,pasa morocha y pasa rubia)', 222, 5, '0000-00-00', 'images/mixes/pic06.jpg', 'sin-foto2.png'),
(95, 'Mix de frutas secas sin pasas', 'Presentación x 500g (nuez,almendra,caju,avellana o castañas de para)', 565, 5, '0000-00-00', 'images/mixes/pic07.jpg', 'sin-foto2.png'),
(96, 'Mix picada x 500g', '(almendra, maiz frito salado, nuez,mani Japones,habas fritas saladas y semilla de girasol tostadas con sal)', 238, 5, '0000-00-00', 'images/mixes/pic08.jpg', 'sin-foto2.png'),
(97, 'Mix tropical x 500g', '(nuez,almendra,caju,pasa morocha, pasa rubia, banana, anana y papaya)', 286, 5, '0000-00-00', 'images/mixes/pic09.jpg', 'sin-foto2.png'),
(98, 'Mix de frutas secas español', 'Presentación x 500g Nuez,Almendra,Castaña de Caju,Mani,Nuez Pecan Y Avellana', 378, 5, '0000-00-00', 'images/mixes/pic10.jpg', 'sin-foto2.png'),
(99, 'Aceite de Coco neutro', 'Presentación x 360 cc', 243, 6, '0000-00-00', 'images/veganos/pic02.jpg', 'sin-foto2.png'),
(100, 'Agar Agar', 'Presentación x 100grs Marca: argen diet', 335, 6, '0000-00-00', 'images/veganos/pic03.jpg', 'sin-foto2.png'),
(101, 'Leche de coco en polvo', 'Presentación x 250g', 311, 6, '0000-00-00', 'images/veganos/pic04.jpg', 'sin-foto2.png'),
(102, 'Garbanzo 9mm', 'Presentacion x 500g', 79, 6, '0000-00-00', 'images/veganos/pic05.jpg', 'sin-foto2.png'),
(103, 'Germen de Trigo', 'Presentación x 500g', 55, 6, '0000-00-00', 'images/veganos/pic06.jpg', 'sin-foto2.png'),
(104, 'Harina de Arroz Blanco', 'Presentación x 500g', 35, 6, '0000-00-00', 'images/veganos/pic07.jpg', 'sin-foto2.png'),
(105, 'Harina de lino', 'Presentación x 500g', 60, 6, '0000-00-00', 'images/veganos/pic08.jpg', 'sin-foto2.png'),
(106, 'Harina de garbanzo', 'Presentación x 500g', 30, 6, '0000-00-00', 'images/veganos/pic09.jpg', 'sin-foto2.png'),
(107, 'Pimientas surtidas', 'Presentación x 100g', 108, 6, '0000-00-00', 'images/veganos/pic10.jpg', 'sin-foto2.png'),
(108, 'soja texturizada fina', 'Presentación x 100g', 0, 6, '0000-00-00', 'images/veganos/pic11.jpg', 'sin-foto2.png'),
(109, 'soja texturizada grande', 'Presentación x 500g', 183, 6, '0000-00-00', 'images/veganos/pic12.jpg', 'sin-foto2.png'),
(110, 'soja texturizada mediana', 'Presentación x 500g', 56, 6, '0000-00-00', 'images/veganos/pic13.jpg', 'sin-foto2.png'),
(111, 'Alga Wakame Kinwaya', 'Presentación x 40grs', 135, 6, '0000-00-00', 'images/veganos/pic14.jpg', 'sin-foto2.png'),
(112, 'Tahina zeenny', 'Presentación x 250grs', 411, 6, '0000-00-00', 'images/veganos/pic15.jpg', 'sin-foto2.png'),
(113, 'Almohaditas rellenas de avellanas', 'Presentación x 500g', 145, 7, '0000-00-00', 'images/lasfor/pic02.jpg', 'sin-foto2.png'),
(114, 'Almohaditas rellenas de chocolate', 'Presentación x 500g', 170, 7, '0000-00-00', 'images/lasfor/pic03.jpg', 'sin-foto2.png'),
(115, 'Almohaditas rellenas de frutilla', 'Presentación x 500g', 145, 7, '0000-00-00', 'images/lasfor/pic04.jpg', 'sin-foto2.png'),
(116, 'Almohaditas rellenas de limon', 'Presentación x 500g', 145, 7, '0000-00-00', 'images/lasfor/pic05.jpg', 'sin-foto2.png'),
(117, 'Almohaditas solo salvado', 'Presentación x 500g', 92, 7, '0000-00-00', 'images/lasfor/pic06.jpg', 'sin-foto2.png'),
(118, 'Almohaditas rellenas de menta', 'Presentación x 500g', 145, 7, '0000-00-00', 'images/lasfor/pic07.jpg', 'sin-foto2.png'),
(119, 'Aritos frutales', 'Presentación x 500g', 106, 7, '0000-00-00', 'images/lasfor/pic08.jpg', 'sin-foto2.png'),
(120, 'Bolitas de chocolate', 'Presentación x 500g', 121, 7, '0000-00-00', 'images/lasfor/pic09.jpg', 'sin-foto2.png'),
(121, 'Aritos con miel', 'Presentación x 500g', 95, 7, '0000-00-00', 'images/lasfor/pic10.jpg', 'sin-foto2.png'),
(122, 'Almohaditas saladas rellenas de queso', 'Presentación x 500g', 219, 7, '0000-00-00', 'images/lasfor/pic11.jpg', 'sin-foto2.png'),
(123, 'Almohaditas saladas rellenas de jamón', 'Presentación x 500g', 219, 7, '0000-00-00', 'images/lasfor/pic12.jpg', 'sin-foto2.png'),
(124, 'Bastoncitos de salvado de trigo', 'Presentación x 500g', 100, 7, '0000-00-00', 'images/lasfor/pic13.jpg', 'sin-foto2.png'),
(125, 'Ositos de frutilla', 'Presentación x 500g', 100, 7, '0000-00-00', 'images/lasfor/pic14.jpg', 'sin-foto2.png'),
(126, 'Almohaditas rellenas de chocolate blanco', 'Presentación x 500g', 170, 7, '0000-00-00', 'images/lasfor/pic15.jpg', 'sin-foto2.png'),
(128, 'Granola basica', 'Presentación x 1kg\r\nAvena arrollada, lino, sesamo integral, girasol, copos de maiz sin azucar, miel', 158, 8, '0000-00-00', 'images/granolas/pic02.jpg', 'sin-foto2.png'),
(129, 'Granola premium sin pasas', 'Presentación x 1kg\r\nAvena arrollada, copos de maiz, sésamo integral, lino, girasol, almendra, caju, coco rallado y miel', 268, 8, '0000-00-00', 'images/granolas/pic03.jpg', 'sin-foto2.png'),
(130, 'Granola tropical', 'Presentación x 1kg\r\nAvena arrollada, copos de maiz, sésamo integral, lino, girasol, almendra, caju, pasas de uva morochas, pasas de uva rubias, coco rallado, miel, banana, papaya, anana y manzana', 327, 8, '0000-00-00', 'images/granolas/pic04.jpg', 'sin-foto2.png'),
(131, 'Granola seca', 'Presentación x 1kg\r\nAvena tradicional, copos de maiz sin azucar, almendras y pasas de uva morochas', 203, 8, '0000-00-00', 'images/granolas/pic05.jpg', 'sin-foto2.png'),
(132, 'Granola especial', 'Presentación x 1kg\r\nAvena arrollada, copos de maiz, sésamo integral, lino, girasol, almendra, caju, coco rallado, miel, pasas de uva morochas y rubias, esencia de vainilla y canela molida. Con un 35% mas de frutos secos', 320, 8, '0000-00-00', 'images/granolas/pic06.jpg', 'sin-foto2.png'),
(134, 'Arandanos rojos deshidratados', 'Presentación x 500g', 381, 9, '0000-00-00', 'images/sinazucar/pic02.jpg', 'sin-foto2.png'),
(135, 'Ciruelas presidente medianas', 'Presentación x 1kg', 354, 9, '0000-00-00', 'images/sinazucar/pic03.jpg', 'sin-foto2.png'),
(136, 'Ciruelas con carozo', 'Presentación x 1kg', 185, 9, '0000-00-00', 'images/sinazucar/pic04.jpg', 'sin-foto2.png'),
(137, 'Ciruelas sin carozo bon bon 110/132', 'Presentación x 1kg', 200, 9, '0000-00-00', 'images/sinazucar/pic05.jpg', 'sin-foto2.png'),
(138, 'Damascos turcos', 'Presentación x 500g', 377, 9, '0000-00-00', 'images/sinazucar/pic06.jpg', 'sin-foto2.png'),
(139, 'Datiles Israelies Medjool', 'Presentación x 250g', 327, 9, '0000-00-00', 'images/sinazucar/pic07.jpg', 'sin-foto2.png'),
(140, 'Manzanas chips', 'Presentación x 250g', 147, 9, '0000-00-00', 'images/sinazucar/pic08.jpg', 'sin-foto2.png'),
(141, 'Higos Turcos Lerida N°4 (Turquia)', 'Presentación x 250g', 257, 9, '0000-00-00', 'images/sinazucar/pic09.jpg', 'sin-foto2.png'),
(142, 'Manzana desecada en cubos', 'Presentación x 250g', 197, 9, '0000-00-00', 'images/sinazucar/pic10.jpg', 'sin-foto2.png'),
(143, 'Pasas de uva jumbo morochas', 'Presentación x 1kg', 198, 9, '0000-00-00', 'images/sinazucar/pic11.jpg', 'sin-foto2.png'),
(144, 'Pasas de uva sultanina rubias', 'Presentación x 500g', 211, 9, '0000-00-00', 'images/sinazucar/pic12.jpg', 'sin-foto2.png'),
(145, 'Pasas de uva sultaninas morochas', 'Presentación x 1kg', 182, 9, '0000-00-00', 'images/sinazucar/pic13.jpg', 'sin-foto2.png'),
(146, 'Arandanos Azules deshidratados', 'Presentación x 500g', 444, 9, '0000-00-00', 'images/sinazucar/pic14.jpg', 'sin-foto2.png'),
(147, 'Manzana desecada en rodajas', 'Presentación x 250g', 185, 9, '0000-00-00', 'images/sinazucar/pic15.jpg', 'sin-foto2.png'),
(148, 'Datiles egipcios', 'Presentación x 500g', 130, 9, '0000-00-00', 'images/sinazucar/pic16.jpg', 'sin-foto2.png'),
(149, 'Anana en cubos de colores', 'Presentación x 250g', 221, 10, '0000-00-00', 'images/glaseadas/pic01.jpg', 'sin-foto2.png'),
(150, 'Anana en rodajas', 'Presentación x 500g', 271, 10, '0000-00-00', 'images/glaseadas/pic02.jpg', 'sin-foto2.png'),
(151, 'Banana chips desecada', 'Presentación x 500g', 282, 10, '0000-00-00', 'images/glaseadas/pic03.jpg', 'sin-foto2.png'),
(152, 'Cereza glaseada entera', 'Presentación x 250g', 243, 10, '0000-00-00', 'images/glaseadas/pic04.jpg', 'sin-foto2.png'),
(153, 'Fruta escurrida', 'Presentación x 1kg Frutas surtidas escurridas/abrillantadas (para pan dulce)', 134, 10, '0000-00-00', 'images/glaseadas/pic05.jpg', 'sin-foto2.png'),
(154, 'Fruta glaseada', 'Presentación x 1kg\r\n\r\nFrutas surtidas glaseadas en trozos', 379, 10, '0000-00-00', 'images/glaseadas/pic06.jpg', 'sin-foto2.png'),
(155, 'Fruta escurrida especial', 'Presentación x 1kg\r\n\r\nFrutas surtidas escurridas/abrillantadas ( para pan dulce ) Con cereza y quinoto', 174, 10, '0000-00-00', 'images/glaseadas/pic07.jpg', 'sin-foto2.png'),
(156, 'Fruta nevada trozada', 'Presentación x 1kg', 272, 10, '0000-00-00', 'images/glaseadas/pic08.jpg', 'sin-foto2.png'),
(157, 'Jengibre glaseado en cubos (Tailandia)', 'Presentación x 250g', 189, 10, '0000-00-00', 'images/glaseadas/pic09.jpg', 'sin-foto2.png'),
(158, 'Kiwi en rodajas glaseado', 'Presentación x 250g', 196, 10, '0000-00-00', 'images/glaseadas/pic10.jpg', 'sin-foto2.png'),
(159, 'Papaya en cubos (Tailandia)', 'Presentación x 250g', 251, 10, '0000-00-00', 'images/glaseadas/pic11.jpg', 'sin-foto2.png'),
(160, 'Fruta glaseada surtida ESPECIAL', 'Presentación x 1kg', 501, 10, '0000-00-00', 'images/glaseadas/pic12.jpg', 'sin-foto2.png'),
(161, 'Anana en cubo amarillo', 'Presentación x 250g', 230, 10, '0000-00-00', 'images/glaseadas/pic13.jpg', 'sin-foto2.png'),
(162, 'Jengibre glaseado en rebanadas (Tailandia)', 'Presentación x 250g', 274, 10, '0000-00-00', 'images/glaseadas/pic14.jpg', 'sin-foto2.png'),
(163, 'Habas fritas saladas', 'Presentación x 500g', 260, 11, '0000-00-00', 'images/snacks/pic01.jpg', 'sin-foto2.png'),
(164, 'Maiz crocante sabor «ORIGINAL»', 'Presentación x 500g', 252, 11, '0000-00-00', 'images/snacks/pic02.jpg', 'sin-foto2.png'),
(165, 'Maiz crocante sabor «Barbacoa»', 'Presentación x 500g', 304, 11, '0000-00-00', 'images/snacks/pic03.jpg', 'sin-foto2.png'),
(166, 'Maiz crocante sabor «Mostaza y Miel»', 'Presentación x 500g', 338, 11, '0000-00-00', 'images/snacks/pic04.jpg', 'sin-foto2.png'),
(167, 'Chip de batata', 'Presentación x 80 grs.\r\nCon sal marina ', 55, 11, '0000-00-00', 'images/snacks/pic05.jpg', 'sin-foto2.png'),
(168, 'Chip de papa ', 'Presentación x 80 grs. RUSTICAS con sal marina', 55, 11, '0000-00-00', 'images/snacks/pic06.jpg', 'sin-foto2.png'),
(169, 'Chip de mandioca ', 'Presentación x 80 grs. YUKITAS con sal marina ', 55, 11, '0000-00-00', 'images/snacks/pic07.jpg', 'sin-foto2.png'),
(170, 'Chip de remolacha y batata ', 'Presentación x 80 grs. RAICES con sal marina', 55, 11, '0000-00-00', 'images/snacks/pic08.jpg', 'sin-foto2.png'),
(171, 'Chips de arroz yamani integral ', 'Presentación x 60 grs. QUESO', 50, 11, '0000-00-00', 'images/snacks/pic09.jpg', 'sin-foto2.png'),
(172, 'Chip de zanahoria', 'Presentación x 80 grs con sal marina ', 55, 11, '0000-00-00', 'images/snacks/pic10.jpg', 'sin-foto2.png'),
(173, 'Chip de papa', 'Presentación x 80 grs con pimienta con sal marina ', 55, 11, '0000-00-00', 'images/snacks/pic11.jpg', 'sin-foto2.png'),
(174, 'Tortilla chip de maiz y chia', 'Presentación x 90 grs (NACHOS)', 55, 11, '0000-00-00', 'images/snacks/pic12.jpg', 'sin-foto2.png'),
(175, 'Chips de vegetales – Mix de la Huerta ', 'Presentación x 85 grs.', 54, 11, '0000-00-00', 'images/snacks/pic13.jpg', 'sin-foto2.png'),
(176, 'Chips de vegetales – Mix de la toscana ', 'Presentación x 85 grs.', 59, 11, '0000-00-00', 'images/snacks/pic14.jpg', 'sin-foto2.png'),
(177, 'Chips de arroz yamani integral', 'Presentación x 60 grs. CREMA Y CEBOLLA\r\n', 55, 11, '0000-00-00', 'images/snacks/pic15.jpg', 'sin-foto2.png'),
(178, 'Chips rejilla – Estilo bistró ', 'Presentación x 65 grs.', 59, 11, '0000-00-00', 'images/snacks/pic16.jpg', 'sin-foto2.png'),
(179, 'Chips de remolacha – Crocantes ', 'Presentación x 50 grs.', 43, 11, '0000-00-00', 'images/snacks/pic17.jpg', 'sin-foto2.png'),
(180, 'Batatas rejilla – Estilo bistró ', 'Presentación x 65 grs.', 56, 11, '0000-00-00', 'images/snacks/pic18.jpg', 'sin-foto2.png'),
(181, 'Papas Marca: Nuevo Mundo– Estilo caseras ', 'Presentación x 70 grs.', 43, 11, '0000-00-00', 'images/snacks/pic19.jpg', 'sin-foto2.png'),
(182, 'Barra de frutos secos artesanal', 'Presentación x 28g\r\n–Mani, Quinoa y Chocolate Amargo', 30, 11, '0000-00-00', 'images/snacks/pic20.jpg', 'sin-foto2.png'),
(183, 'Barra de frutos secos artesanal', 'Presentación x 28g\r\n–Almendras, Pasas y Miel', 30, 11, '0000-00-00', 'images/snacks/pic21.jpg', 'sin-foto2.png'),
(184, 'Barra de frutos secos artesanal', 'Presentación x 28g\r\n–Castañas de Caju, Semillas de Zapallo y Sal Marina.', 30, 11, '0000-00-00', 'images/snacks/pic22.jpg', 'sin-foto2.png'),
(186, 'Aceite de Coco ', 'Presentación x 360 cc neutro\r\norigen indonesia,LIBRE DE GLUTEN,sin conservantes,apto consumo humano,apto uso externo', 243, 12, '0000-00-00', 'images/aceites/pic01.jpg', 'sin-foto2.png'),
(187, 'Aceite de mani Marca: King', 'Presentación x 1 litro', 238, 12, '0000-00-00', 'images/aceites/pic02.jpg', 'sin-foto2.png'),
(188, 'Aceite de coco neutro', 'Presentación x 250 cc\r\nMarca:Materia Prima', 184, 12, '0000-00-00', 'images/aceites/pic03.jpg', 'sin-foto2.png'),
(189, 'Aceite de coco neutro', 'Presentación x 250cc\r\nVITACOCO', 128, 12, '0000-00-00', 'images/aceites/pic04.jpg', 'sin-foto2.png'),
(190, 'Aceite de coco neutro ', 'Presentación x 660 cc\r\nVITACOCO', 260, 12, '0000-00-00', 'images/aceites/pic05.jpg', 'sin-foto2.png'),
(191, 'Aceite de coco neutro ', 'Presentación x 360cc\r\nVITACOCO ', 162, 12, '0000-00-00', 'images/aceites/pic06.jpg', 'sin-foto2.png'),
(192, 'Sal marina fina ', 'Presentación x 500 grs\r\nMarca : Macrozen', 108, 13, '0000-00-00', 'images/sales/pic01.jpg', 'sin-foto2.png'),
(193, 'Sal marina gruesa a granel', 'Presentación x 1kg', 194, 13, '0000-00-00', 'images/sales/pic02.jpg', 'sin-foto2.png'),
(194, 'Sal marina chimi y tomillo', 'Presentación x 250g \r\nsaborizada con chimichurri y tomillo en doy pack', 113, 13, '0000-00-00', 'images/sales/pic03.jpg', 'sin-foto2.png'),
(195, 'Sal negra', 'Presentación x 500g ', 194, 13, '0000-00-00', 'images/sales/pic04.jpg', 'sin-foto2.png'),
(196, 'Azucar impalpable ', 'Presentación x 1kg\r\nCOVENTRY', 123, 14, '0000-00-00', 'images/endulzantes/pic01.jpg', 'sin-foto2.png'),
(197, 'Azucar negra ', 'Presentación x 1kg\r\nCOVENTRY', 123, 14, '0000-00-00', 'images/endulzantes/pic02.jpg', 'sin-foto2.png'),
(198, 'Azucar rubia ', 'Presentación x 1kg\r\nCOVENTRY', 123, 14, '0000-00-00', 'images/endulzantes/pic03.jpg', 'sin-foto2.png'),
(199, 'Azucar orgánica a granel', 'Presentación x 1kg\r\nMarca : San Isidro', 110, 14, '0000-00-00', 'images/endulzantes/pic15.jpg', 'sin-foto2.png'),
(200, 'Miel multifloral pura ', 'Presentación en envase de plastico x 1 kg', 316, 14, '0000-00-00', 'images/endulzantes/pic05.jpg', 'sin-foto2.png'),
(201, 'Miel multifloral pura ', 'Presentación en envase de plastico x 500g', 190, 14, '0000-00-00', 'images/endulzantes/pic06.jpg', 'sin-foto2.png'),
(202, 'Miel multifloral ', 'Presentación en balde x 15 kg', 2999, 14, '0000-00-00', 'images/endulzantes/pic16.jpg', 'sin-foto2.png'),
(203, 'Polen de flor', 'Presentación x 100g', 142, 14, '0000-00-00', 'images/endulzantes/pic07.jpg', 'sin-foto2.png'),
(204, 'stevia en hojas', 'Presentación x 100g', 111, 14, '0000-00-00', 'images/endulzantes/pic08.jpg', 'sin-foto2.png'),
(205, 'Stevia Liquida', 'Presentación x 100ml \r\nSIN TACC', 122, 14, '0000-00-00', 'images/endulzantes/pic9.jpg', 'sin-foto2.png'),
(206, 'Stevia en Polvo', 'Presentación x 100grs\r\nSIN TACC', 206, 14, '0000-00-00', 'images/endulzantes/pic10.jpg', 'sin-foto2.png'),
(207, 'Azucar saborizada ', 'Presentación en doy pack x 250 grs', 90, 14, '0000-00-00', 'images/endulzantes/pic11.jpg', 'sin-foto2.png'),
(208, 'Azucar integral de coco', 'Presentación x 250g', 202, 14, '0000-00-00', 'images/endulzantes/pic12.jpg', 'sin-foto2.png'),
(209, 'Azucar negra ALZOL', 'Presentación x 1kg', 118, 14, '0000-00-00', 'images/endulzantes/pic13.jpg', 'sin-foto2.png'),
(210, 'Azucar rubia ALZOL', 'Presentación x 1kg', 118, 14, '0000-00-00', 'images/endulzantes/pic14.jpg', 'sin-foto2.png'),
(211, 'Balón de cereal con cobertura de leche', 'Presentación x 80 grs.', 62, 15, '0000-00-00', 'images/confituras/pic01.jpg', 'sin-foto2.png'),
(212, 'Granos de cafe con cobertura semiamarga .', 'Presentación x 80 grs', 75, 15, '0000-00-00', 'images/confituras/pic02.jpg', 'sin-foto2.png'),
(213, 'Avellanas con cobertura de leche', 'Presentación x 80 grs', 102, 15, '0000-00-00', 'images/confituras/pic03.jpg', 'sin-foto2.png'),
(214, 'Almendras con cobertura de leche', 'Presentación x 80 grs.', 94, 15, '0000-00-00', 'images/confituras/pic04.jpg', 'sin-foto2.png'),
(215, 'Arándanos con cobertura semiamarga', 'Presentación x 80 grs', 102, 15, '0000-00-00', 'images/confituras/pic05.jpg', 'sin-foto2.png'),
(216, 'Chocolate semiamargo con relleno sabor menta', 'Presentación x 80 grs', 104, 15, '0000-00-00', 'images/confituras/pic06.jpg', 'sin-foto2.png'),
(217, 'Pasas de uva con cobertura de leche', 'Presentación x 80 grs', 64, 15, '0000-00-00', 'images/confituras/pic07.jpg', 'sin-foto2.png'),
(218, 'Naranjas con cobertura semiamarga', 'Presentación x 80 grs', 102, 15, '0000-00-00', 'images/confituras/pic08.jpg', 'sin-foto2.png'),
(219, 'Maní con cobertura de chocolate semiamargo', 'Presentación x 80 grs', 47, 15, '0000-00-00', 'images/confituras/pic09.jpg', 'sin-foto2.png'),
(220, 'Lentejas de chocolate confitado', 'Presentación x 80 grs', 54, 15, '0000-00-00', 'images/confituras/pic10.jpg', 'sin-foto2.png'),
(221, 'Granos de café con cobertura semiamarga', 'Presentación x 500 grs.', 324, 15, '0000-00-00', 'images/confituras/pic11.jpg', 'sin-foto2.png'),
(222, 'Avellanas con cobertura de leche', 'Presentación x 500 grs.', 407, 15, '0000-00-00', 'images/confituras/pic12.jpg', 'sin-foto2.png'),
(223, 'Almendras con cobertura de leche ', 'Presentación x 500 grs.', 393, 15, '0000-00-00', 'images/confituras/pic13.jpg', 'sin-foto2.png'),
(224, 'Naranjas con cobertura semiamarga ', 'Presentación x 500 grs.', 407, 15, '0000-00-00', 'images/confituras/pic14.jpg', 'sin-foto2.png'),
(226, 'Agar Agar', 'Presentación x 100g', 335, 16, '0000-00-00', 'images/reposteria/pic01.jpg', 'sin-foto2.png'),
(227, 'Bicarbonato de sodio', 'Presentación x 1kg', 103, 16, '0000-00-00', 'images/reposteria/pic02.jpg', 'sin-foto2.png'),
(228, 'Canela en rama H2', 'Presentación x 50g', 147, 16, '0000-00-00', 'images/reposteria/pic03.jpg', 'sin-foto2.png'),
(229, 'Canela molida', 'Presentación x 100g', 68, 16, '0000-00-00', 'images/reposteria/pic04.jpg', 'sin-foto2.png'),
(230, 'Cerezas al marrasquino enteras lata', 'Presentación x 3,1 kg ', 961, 16, '0000-00-00', 'images/reposteria/pic05.jpg', 'sin-foto2.png'),
(231, 'Chaucha de Vainilla Entera', 'Presentación x Unidad', 269, 16, '0000-00-00', 'images/reposteria/pic06.jpg', 'sin-foto2.png'),
(232, 'Chocolate familiar para taza SIN TACC', 'Presentación x 100 grs ', 128, 16, '0000-00-00', 'images/reposteria/pic07.jpg', 'sin-foto2.png'),
(233, 'Polvo de hornear', 'Presentación x 1kg', 203, 16, '0000-00-00', 'images/reposteria/pic08.jpg', 'sin-foto2.png'),
(234, 'Dulce de Leche c/Stevia', 'Presentación por 200 grs – SIN TACC', 155, 16, '0000-00-00', 'images/reposteria/pic09.jpg', 'sin-foto2.png'),
(235, 'Cacao amargo puro alcalino', 'Presentación x 500g', 236, 16, '0000-00-00', 'images/reposteria/pic10.jpg', 'sin-foto2.png'),
(236, 'Gelatina sin sabor', 'Presentación x 100g', 132, 16, '0000-00-00', 'images/reposteria/pic11.jpg', 'sin-foto2.png'),
(237, 'Baño de reposteria semiamargo', 'Presentación x 1kg', 290, 16, '0000-00-00', 'images/reposteria/pic12.jpg', 'sin-foto2.png'),
(238, 'Pasta de mani', 'Presentación x 485 grs', 157, 16, '0000-00-00', 'images/reposteria/pic13.jpg', 'sin-foto2.png'),
(239, 'Yerba mate organica RoaPipo', 'Presentación x 1 Kg ', 344, 17, '0000-00-00', 'images/yerbas/pic01.jpg', 'sin-foto2.png'),
(240, 'Yerba mate organica RoaPipo Tradicional', 'Presentación x 500 grs ', 169, 17, '0000-00-00', 'images/yerbas/pic02.jpg', 'sin-foto2.png'),
(241, 'Yerba mate Sin TACC Kalena ', 'Presentación x 500g', 158, 17, '0000-00-00', 'images/yerbas/pic03.jpg', 'sin-foto2.png'),
(242, 'Yerba mate Ajedrez', 'Presentación x 500 grs', 131, 17, '0000-00-00', 'images/yerbas/pic04.jpg', 'sin-foto2.png'),
(243, 'Yerba con te verde Jesper', 'Presentación x 500 grs', 148, 17, '0000-00-00', 'images/yerbas/pic05.jpg', 'sin-foto2.png'),
(244, 'Yerba con hierbas Jesper ', 'Presentación x 500g', 169, 17, '0000-00-00', 'images/yerbas/pic06.jpg', 'sin-foto2.png'),
(245, 'Yerba mate orgánica Taihang', 'Presentación x 500 grs. ', 164, 17, '0000-00-00', 'images/yerbas/pic07.jpg', 'sin-foto2.png'),
(246, 'Yerba mate organica RoaPipo ', 'Presentación x 500 grs – Suave', 169, 17, '0000-00-00', 'images/yerbas/pic08.jpg', 'sin-foto2.png'),
(247, 'Yerba mate SIN TACC KALENA ', 'Presentación x 2 kg ', 596, 17, '0000-00-00', 'images/yerbas/pic09.jpg', 'sin-foto2.png'),
(248, 'Galletas de arroz Carilo sin sal nuevas  SIN TACC', 'Presentación x 150 grs ', 56, 18, '0000-00-00', 'images/arroz/pic01.jpg', 'sin-foto2.png'),
(249, 'Galletas de arroz Carilo dulces – SIN TACC', 'Presentación x 150 grs ', 56, 18, '0000-00-00', 'images/arroz/pic02.jpg', 'sin-foto2.png'),
(250, 'Galletas de arroz Carilo tradicional – SIN TACC', 'Presentación x 150 grs ', 56, 18, '0000-00-00', 'images/arroz/pic03.jpg', 'sin-foto2.png'),
(251, 'Galletas de arroz Carilo light – SIN TACC', 'Presentación x 150 grs', 56, 18, '0000-00-00', 'images/arroz/pic04.jpg', 'sin-foto2.png'),
(252, 'Tostaditas de arroz', 'Presentación x 120 grs', 48, 18, '0000-00-00', 'images/arroz/pic05.jpg', 'sin-foto2.png'),
(253, 'Galletas de arroz Carilo Chocolate – SIN TACC', 'Presentación x 150 grs', 56, 18, '0000-00-00', 'images/arroz/pic06.jpg', 'sin-foto2.png'),
(254, 'Chips de arroz yamani integral CREMA Y CEBOLLA', 'Presentación x 60 grs. ', 50, 18, '0000-00-00', 'images/arroz/pic07.jpg', 'sin-foto2.png'),
(255, 'Chips de arroz yamani integral QUESO', 'Presentación x 60 grs. ', 50, 18, '0000-00-00', 'images/arroz/pic08.jpg', 'sin-foto2.png'),
(256, 'Tostadas de arroz integral con sal Molinos Ala', 'Presentación x 150 grs.', 45, 18, '0000-00-00', 'images/arroz/pic09.jpg', 'sin-foto2.png'),
(257, 'Galletas de arroz yamani integral con sal Rodez', 'Presentación x 75 grs. ', 38, 18, '0000-00-00', 'images/arroz/pic10.jpg', 'sin-foto2.png'),
(258, 'Galletas de arroz yamani integral sin sal Rodez', 'Presentación x 75 grs.', 38, 18, '0000-00-00', 'images/arroz/pic11.jpg', 'sin-foto2.png'),
(259, 'Galletas de arroz yamani integral Rodez', 'Presentación x 75 grs. \r\nCon sarraceno, quinoa y amaranto', 49, 18, '0000-00-00', 'images/arroz/pic12.jpg', 'sin-foto2.png'),
(260, 'Galletas de arroz yamani integral Rodez', 'Presentación x 75 grs.\r\nCon chia, lino y sesamo ', 42, 18, '0000-00-00', 'images/arroz/pic13.jpg', 'sin-foto2.png'),
(261, 'Tostadas de arroz integral sin sal Molinos Ala', 'Presentación x 150 grs. ', 45, 18, '0000-00-00', 'images/arroz/pic14.jpg', 'sin-foto2.png'),
(262, 'Tostadas de arroz integral original Molinos Ala', 'Presentación x 150 grs. ', 43, 18, '0000-00-00', 'images/arroz/pic15.jpg', 'sin-foto2.png'),
(263, 'Fariña de mandioca', 'Presentación x 1kg', 137, 19, '0000-00-00', 'images/harinas/pic01.jpg', 'sin-foto2.png'),
(264, 'Fecula de papa', 'Presentación x 1kg', 260, 19, '0000-00-00', 'images/harinas/pic02.jpg', 'sin-foto2.png'),
(265, 'Fecula o almidon de maiz', 'Presentación x 1kg', 82, 19, '0000-00-00', 'images/harinas/pic03.jpg', 'sin-foto2.png'),
(266, 'Fecula de mandioca', 'Presentación x 1kg', 128, 19, '0000-00-00', 'images/harinas/pic04.jpg', 'sin-foto2.png'),
(267, 'Harina de arroz blanco', 'Presentación x 1kg', 65, 19, '0000-00-00', 'images/harinas/pic05.jpg', 'sin-foto2.png'),
(268, 'Harina de chia', 'Presentación x 1kg', 89, 19, '0000-00-00', 'images/harinas/pic06.jpg', 'sin-foto2.png'),
(269, 'Harina de garbanzo', 'Presentación x 1kg', 54, 19, '0000-00-00', 'images/harinas/pic07.jpg', 'sin-foto2.png'),
(270, 'Harina de lino', 'Presentación x 1kg', 117, 19, '0000-00-00', 'images/harinas/pic08.jpg', 'sin-foto2.png'),
(271, 'Harina de lentejas', 'Presentación x 1kg', 91, 19, '0000-00-00', 'images/harinas/pic09.jpg', 'sin-foto2.png'),
(272, 'Harina de avena', 'Presentación x 1kg', 113, 19, '0000-00-00', 'images/harinas/pic10.jpg', 'sin-foto2.png'),
(273, 'Harina de arveja', 'Presentación x 1kg', 78, 19, '0000-00-00', 'images/harinas/pic11.jpg', 'sin-foto2.png'),
(274, 'Gluten Puro', 'Presentación x 500g', 306, 19, '0000-00-00', 'images/harinas/pic12.jpg', 'sin-foto2.png'),
(275, 'Harina de maní', 'Presentación x 1kg', 106, 19, '0000-00-00', 'images/harinas/pic13.jpg', 'sin-foto2.png'),
(276, 'Mix de semillas molidas', 'Presentación x 1kg', 103, 19, '0000-00-00', 'images/harinas/pic14.jpg', 'sin-foto2.png'),
(277, 'Harina de amaranto', 'Presentación x 500g', 91, 19, '0000-00-00', 'images/harinas/pic15.jpg', 'sin-foto2.png'),
(278, 'Leche de almendras Tratenfu ORIGINAL ', 'Presentación x 1 litro envase tetra brick', 167, 20, '0000-00-00', 'images/leches/pic01.jpg', 'sin-foto2.png'),
(279, 'Leche de almendras Tratenfu VAINILLA ', 'Presentación x 1 litro envase tetra brick', 167, 20, '0000-00-00', 'images/leches/pic02.jpg', 'sin-foto2.png'),
(280, 'Leche de caju Tratenfu VAINILLA', 'Presentación x 1 litro ', 167, 20, '0000-00-00', 'images/leches/pic03.jpg', 'sin-foto2.png'),
(281, 'Leche de almendras Vrink ORIGINAL', 'Presentación x 1 litro ', 146, 20, '0000-00-00', 'images/leches/pic04.jpg', 'sin-foto2.png'),
(282, 'Leche de almendras Vrink CHOCOLATE', 'Presentación x 1 litro ', 166, 20, '0000-00-00', 'images/leches/pic05.jpg', 'sin-foto2.png'),
(283, 'Leche de almendras Vrink VAINILLA', 'Presentación x 1 litro ', 153, 20, '0000-00-00', 'images/leches/pic06.jpg', 'sin-foto2.png'),
(284, 'Leche de almendras y coco Blend', 'Presentación x 1 litro', 167, 20, '0000-00-00', 'images/leches/pic07.jpg', 'sin-foto2.png'),
(285, 'Leche de almendras Tratenfu CHOCOLATE', 'Presentación x 1 litro', 167, 20, '0000-00-00', 'images/leches/pic08.jpg', 'sin-foto2.png'),
(286, 'Leche de mani CHOCOLATE', 'Presentación x 1 litro', 132, 20, '0000-00-00', 'images/leches/pic09.jpg', 'sin-foto2.png'),
(287, 'Leche de mani ORIGINAL', 'Presentación x 1 litro', 118, 20, '0000-00-00', 'images/leches/pic10.jpg', 'sin-foto2.png'),
(288, 'Leche de almendras Vrink SIN AZUCAR ORIGINAL', 'Presentación x 1 litro ', 146, 20, '0000-00-00', 'images/leches/pic11.jpg', 'sin-foto2.png'),
(289, 'Avena gruesa', 'Presentación x 1kg', 85, 21, '0000-00-00', 'images/avenas/pic01.jpg', 'sin-foto2.png'),
(290, 'Avena tradicional', 'Presentación x 1kg', 73, 21, '0000-00-00', 'images/avenas/pic02.jpg', 'sin-foto2.png'),
(291, 'Avena extra fina', 'Presentación x 1kg', 103, 21, '0000-00-00', 'images/avenas/pic03.jpg', 'sin-foto2.png'),
(292, 'Jugo exprimido MANZANA ROJA ', 'Presentación x 1000 ml', 104, 22, '0000-00-00', 'images/jugos/pic01.jpg', 'sin-foto2.png'),
(293, 'Jugo exprimido MANZANA VERDE', 'Presentación x 1000 ml', 104, 22, '0000-00-00', 'images/jugos/pic02.jpg', 'sin-foto2.png'),
(294, 'Jugo exprimido FRUTILLA Y MANZANA', 'Presentación x 1000 ml', 130, 22, '0000-00-00', 'images/jugos/pic03.jpg', 'sin-foto2.png'),
(295, 'Jugo exprimido ARANDANOS Y MANZANA ', 'Presentación x 1000 ml', 130, 22, '0000-00-00', 'images/jugos/pic04.jpg', 'sin-foto2.png'),
(296, 'Jugo exprimido MANZANA VERDE ', 'Presentación x 200 ml', 36, 22, '0000-00-00', 'images/jugos/pic05.jpg', 'sin-foto2.png'),
(297, 'Jugo exprimido MANZANA ROJA ', 'Presentación x 200 ml', 36, 22, '0000-00-00', 'images/jugos/pic06.jpg', 'sin-foto2.png'),
(298, 'Jugo exprimido MANZANA ORGANICA ', 'Presentación x 1 lt', 156, 22, '0000-00-00', 'images/jugos/pic07.jpg', 'sin-foto2.png'),
(299, 'Jugo exprimido NARANJA ', 'Presentación x 1 lt', 108, 22, '0000-00-00', 'images/jugos/pic08.jpg', 'sin-foto2.png'),
(301, 'Pimienta blanca en grano', 'Presentación x 100g', 140, 23, '0000-00-00', 'images/condimentos/pic01.jpg', 'sin-foto2.png'),
(302, 'Pimienta Jamaica en grano', 'Presentación x 100g', 137, 23, '0000-00-00', 'images/condimentos/pic02.jpg', 'sin-foto2.png'),
(303, 'Pimienta negra en grano', 'Presentación x 100g', 73, 23, '0000-00-00', 'images/condimentos/pic03.jpg', 'sin-foto2.png'),
(304, 'Pimienta rosa en grano', 'Presentación x 100g', 509, 23, '0000-00-00', 'images/condimentos/pic04.jpg', 'sin-foto2.png'),
(305, 'Pimientas surtidas', 'Presentación x 100g', 108, 23, '0000-00-00', 'images/condimentos/pic05.jpg', 'sin-foto2.png'),
(306, 'Adobo para pizza Primera Calidad', 'Presentación x 100g', 49, 23, '0000-00-00', 'images/condimentos/pic06.jpg', 'sin-foto2.png'),
(307, 'Aji de cayena', 'Presentación x 100g', 65, 23, '0000-00-00', 'images/condimentos/pic07.jpg', 'sin-foto2.png'),
(308, 'Aji molido', 'Presentación x 100g', 63, 23, '0000-00-00', 'images/condimentos/pic08.jpg', 'sin-foto2.png'),
(309, 'Ajinomoto (glutamato de sodio)', 'Presentación x 100g', 48, 23, '0000-00-00', 'images/condimentos/pic09.jpg', 'sin-foto2.png'),
(310, 'Ajo granulado', 'Presentación x 100g', 69, 23, '0000-00-00', 'images/condimentos/pic10.jpg', 'sin-foto2.png'),
(311, 'Anis', 'Presentación x 100g', 51, 23, '0000-00-00', 'images/condimentos/pic11.jpg', 'sin-foto2.png'),
(312, 'Azafrán PRUGNA', 'Presentación x 2g', 61, 23, '0000-00-00', 'images/condimentos/pic12.jpg', 'sin-foto2.png'),
(313, 'Cardamomo', 'Presentación x 50g', 330, 23, '0000-00-00', 'images/condimentos/pic13.jpg', 'sin-foto2.png'),
(314, 'Cebolla en polvo', 'Presentación x 100g', 90, 23, '0000-00-00', 'images/condimentos/pic14.jpg', 'sin-foto2.png'),
(315, 'Chimichurry', 'Presentación x 100g (Aji, Ajo, Perejil, Oregano y Pimenton Deshidratados)', 57, 23, '0000-00-00', 'images/condimentos/pic15.jpg', 'sin-foto2.png'),
(316, 'Clavo de olor', 'Presentación x 100g', 249, 23, '0000-00-00', 'images/condimentos/pic16.jpg', 'sin-foto2.png'),
(317, 'Comino en grano', 'Presentación x 100g', 76, 23, '0000-00-00', 'images/condimentos/pic17.jpg', 'sin-foto2.png'),
(318, 'Comino Molido', 'Presentación x 1kg', 413, 23, '0000-00-00', 'images/condimentos/pic18.jpg', 'sin-foto2.png'),
(319, 'Coriandro molido', 'Presentación x 100g', 23, 23, '0000-00-00', 'images/condimentos/pic19.jpg', 'sin-foto2.png'),
(320, 'Coriandro en grano', 'Presentación x 1kg', 114, 23, '0000-00-00', 'images/condimentos/pic20.jpg', 'sin-foto2.png'),
(321, 'Curcuma', 'Presentación x 100g', 73, 23, '0000-00-00', 'images/condimentos/pic21.jpg', 'sin-foto2.png'),
(322, 'Curry suave', 'Presentación x 100g', 53, 23, '0000-00-00', 'images/condimentos/pic22.jpg', 'sin-foto2.png'),
(323, 'Especias surtidas', 'Presentación x 100g (comino, azafran, aji molido, pimienta, romero, pimenton, curry)', 75, 23, '0000-00-00', 'images/condimentos/pic23.jpg', 'sin-foto2.png'),
(324, 'Jengibre en raiz', 'Presentación x 100g', 80, 23, '0000-00-00', 'images/condimentos/pic24.jpg', 'sin-foto2.png'),
(325, 'Jengibre molido', 'Presentación x 100g', 86, 23, '0000-00-00', 'images/condimentos/pic25.jpg', 'sin-foto2.png'),
(326, 'Laurel', 'Presentación x 100g', 89, 23, '0000-00-00', 'images/condimentos/pic26.jpg', 'sin-foto2.png'),
(327, 'Nuez moscada en grano', 'Presentación x 50g', 114, 23, '0000-00-00', 'images/condimentos/pic27.jpg', 'sin-foto2.png'),
(328, 'Nuez moscada molida', 'Presentación x 100g', 94, 23, '0000-00-00', 'images/condimentos/pic28.jpg', 'sin-foto2.png'),
(329, 'Paprika', 'Presentación x 100g', 88, 23, '0000-00-00', 'images/condimentos/pic29.jpg', 'sin-foto2.png'),
(330, 'Pimenton ahumado', 'Presentación x 100g', 70, 23, '0000-00-00', 'images/condimentos/pic30.jpg', 'sin-foto2.png'),
(331, 'Pimenton dulce', 'Presentación x 100g', 54, 23, '0000-00-00', 'images/condimentos/pic31.jpg', 'sin-foto2.png'),
(332, 'Provenzal', 'Presentación x 100g (Perejil y ajo deshidratado)', 57, 23, '0000-00-00', 'images/condimentos/pic32.jpg', 'sin-foto2.png'),
(333, 'Semilla de mostaza amarilla molida', 'Presentación x 100g', 64, 23, '0000-00-00', 'images/condimentos/pic33.jpg', 'sin-foto2.png'),
(334, 'Wasabi en polvo', 'Presentación x 100g', 170, 23, '0000-00-00', 'images/condimentos/pic34.jpg', 'sin-foto2.png'),
(336, 'Cebolla en escamas', 'Presentación x 100g', 76, 24, '0000-00-00', 'images/verduras/pic01.jpg', 'sin-foto2.png'),
(337, 'Espinaca deshidratada', 'Presentación x 100g', 83, 24, '0000-00-00', 'images/verduras/pic02.jpg', 'sin-foto2.png'),
(338, 'Juliana especial', 'Presentación x 100g', 86, 24, '0000-00-00', 'images/verduras/pic03.jpg', 'sin-foto2.png'),
(339, 'Pure de papas en escamas', 'Presentación x 500g', 154, 24, '0000-00-00', 'images/verduras/pic04.jpg', 'sin-foto2.png'),
(340, 'Tomates secos premium', 'Presentación x 500g', 345, 24, '0000-00-00', 'images/verduras/pic05.jpg', 'sin-foto2.png'),
(341, 'Tomates secos', 'Presentación x 500g', 288, 24, '0000-00-00', 'images/verduras/pic06.jpg', 'sin-foto2.png'),
(342, 'Hongos secos Boletus', 'Presentación x 250g', 258, 24, '0000-00-00', 'images/verduras/pic07.jpg', 'sin-foto2.png'),
(343, 'Hongos shitake enteros', 'Presentación x 100g', 266, 24, '0000-00-00', 'images/verduras/pic08.jpg', 'sin-foto2.png'),
(344, 'Hongos girgolas enteros', 'Presentación x 100g', 228, 24, '0000-00-00', 'images/verduras/pic09.jpg', 'sin-foto2.png'),
(346, 'Harina Integral de trigo sarraceno', 'Presentación x 1kg', 183, 19, '0000-00-00', 'images/harinas/pic16.jpg', 'sin-foto2.png'),
(347, 'Harina Integral trigo sarraceno SIN TACC', 'Presentación x 500 grs', 103, 19, '0000-00-00', 'images/harinas/pic17.jpg', 'sin-foto2.png'),
(348, 'Arroz Doble Carolina blanco 00000', 'Presentación x 1kg', 95, 25, '0000-00-00', 'images/arroces/pic01.jpg', 'sin-foto2.png'),
(349, 'Arroz yamani integral', 'Presentación x 1kg', 100, 25, '0000-00-00', 'images/arroces/pic02.jpg', 'sin-foto2.png'),
(350, 'Arroz para sushi', 'Presentación x 1kg', 101, 25, '0000-00-00', 'images/arroces/pic03.jpg', 'sin-foto2.png'),
(351, 'Arroz blanco largo fino', 'Presentación x 1kg', 66, 25, '0000-00-00', 'images/arroces/pic04.jpg', 'sin-foto2.png'),
(352, 'Arroz basmati', 'Presentación x 500g', 123, 25, '0000-00-00', 'images/arroces/pic05.jpg', 'sin-foto2.png'),
(353, 'Coco en cubos', 'Presentación x 250g', 387, 26, '0000-00-00', 'images/coco/pic01.jpg', 'sin-foto2.png'),
(354, 'Coco rallado fino', 'Presentación x 500g', 147, 26, '0000-00-00', 'images/coco/pic02.jpg', 'sin-foto2.png'),
(355, 'Leche de coco en polvo', 'Presentación x 250g', 311, 26, '0000-00-00', 'images/coco/pic03.jpg', 'sin-foto2.png'),
(356, 'Leche de coco en lata', 'Presentación x 400ml', 224, 26, '0000-00-00', 'images/coco/pic04.jpg', 'sin-foto2.png'),
(357, 'Coco en escamas FLAKES', 'Presentación x 500g', 371, 26, '0000-00-00', 'images/coco/pic05.jpg', 'sin-foto2.png'),
(358, 'Agua de coco ', 'Presentación x 1 litro', 166, 26, '0000-00-00', 'images/coco/pic06.jpg', 'sin-foto2.png'),
(359, 'Azucar integral de coco', 'Presentación x 250g', 202, 26, '0000-00-00', 'images/coco/pic07.jpg', 'sin-foto2.png'),
(360, 'Harina de coco', 'Presentación x 500', 304, 26, '0000-00-00', 'images/coco/pic08.jpg', 'sin-foto2.png'),
(361, 'Fideos de harina integral Con algas de la patagoni', 'Presentación x 500 grs\r\n', 106, 27, '0000-00-00', 'images/fideos/pic01.jpg', 'sin-foto2.png'),
(362, 'Fideos de harina integral Con zanahoria', 'Presentación x 500 grs\r\n', 106, 27, '0000-00-00', 'images/fideos/pic02.jpg', 'sin-foto2.png'),
(363, 'Fideos de harina integral Con tomate', 'Presentación x 500 grs\r\n', 106, 27, '0000-00-00', 'images/fideos/pic03.jpg', 'sin-foto2.png'),
(364, 'Fideos de harina integral con germen de trigo y mi', 'Presentación x 500 grs\r\n', 106, 27, '0000-00-00', 'images/fideos/pic04.jpg', 'sin-foto2.png'),
(365, 'Fideos de arroz tradicional ', 'Presentación x 300g', 95, 27, '0000-00-00', 'images/fideos/pic05.jpg', 'sin-foto2.png'),
(366, 'Fideos de arroz con maiz', 'Presentación x 300g', 95, 27, '0000-00-00', 'images/fideos/pic06.jpg', 'sin-foto2.png'),
(367, 'soja texturizada fina', 'Presentación x 500g', 100, 28, '0000-00-00', 'images/soja/pic01.jpg', 'sin-foto2.png'),
(368, 'soja texturizada grande', 'Presentación x 500g', 326, 28, '0000-00-00', 'images/soja/pic02.jpg', 'sin-foto2.png'),
(369, 'soja texturizada mediana', 'Presentación x 500g', 56, 28, '0000-00-00', 'images/soja/pic03.jpg', 'sin-foto2.png'),
(370, 'Granos de Cafe verde sin tostar Brasil', 'Presentación x 250g\r\n', 175, 1, '0000-00-00', 'images/semillas/pic15.jpg', 'sin-foto2.png'),
(371, 'Mix de 4 semillas', 'Presentación x 1kg (lino, sesamo integral, girasol, chia)', 200, 1, '0000-00-00', 'images/semillas/pic16.jpg', 'sin-foto2.png'),
(372, 'Mix de semillas premium', 'Presentación x 1kg (girasol, zapallo, amapola , chia, lino, sesamo integral y sesamo blanco)\r\n', 360, 1, '0000-00-00', 'images/semillas/pic17.jpg', 'sin-foto2.png'),
(373, 'Mix de semillas INKA-O', 'Presentación x 1kg (amaranto, amapola, chia, lino, sesamo negro y sesamo integral)', 340, 1, '0000-00-00', 'images/semillas/pic18.jpg', 'sin-foto2.png'),
(374, 'Semilla de sesamo integral India', 'Presentación x 1kg', 245, 1, '0000-00-00', 'images/semillas/pic19.jpg', 'sin-foto2.png'),
(375, 'Semilla de quinoa Nacional', 'Presentación x 1kg', 380, 1, '0000-00-00', 'images/Semillas/pic20.jpg', 'sin-foto2.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_pedido`
--

CREATE TABLE `producto_pedido` (
  `id_producto_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `confirmar_contra` varchar(50) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `direccion` varchar(20) NOT NULL,
  `piso` varchar(20) NOT NULL,
  `dpto` int(20) NOT NULL,
  `localidad` varchar(20) NOT NULL,
  `codigo_postal` int(4) NOT NULL,
  `telefono` int(15) NOT NULL,
  `comentarios` text NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `contrasena`, `confirmar_contra`, `nombre`, `direccion`, `piso`, `dpto`, `localidad`, `codigo_postal`, `telefono`, `comentarios`, `email`) VALUES
(1, 'rolanditorepiola', '123', '123', 'Rolando', 'qweqwe', '12', 0, 'qweqwe', 12, 1212, 'qwewqeq', 'elviralamisionera@hotmail.com'),
(2, 'comprador', '123', '123', 'rolando', 'av la plata', '2', 4, 'caba', 1234, 12341234, 'asd', 'rolandogimenez@gmail.com'),
(3, 'rolanditorep', '123', '123', '123', '123', '123', 123, '123', 123, 123, '123', '123'),
(4, 'rolanditor', '123', '123', '123', '123', '123', 123, '123', 123, 123, '123', '123123'),
(5, 'comprador1', '123', '123', 'compi', 'compi', 'compi', 0, ' compi', 0, 12341234, 'sdasa', 'compi');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  ADD PRIMARY KEY (`codigoComida`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`codigoComida`);

--
-- Indices de la tabla `producto_pedido`
--
ALTER TABLE `producto_pedido`
  ADD PRIMARY KEY (`id_producto_pedido`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `cat_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ofertas`
--
ALTER TABLE `ofertas`
  MODIFY `codigoComida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=372;

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `codigoComida` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=376;

--
-- AUTO_INCREMENT de la tabla `producto_pedido`
--
ALTER TABLE `producto_pedido`
  MODIFY `id_producto_pedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
